## State

  Work in progress

## About

This is a client side of app that goal is to handle your debt.

The goal of the project is to create simple app that will handle for you your debts.
So you are able to create common wallet with your colleagues where you will store your debt.

Of course this do not make sense, but it is for educational purpose.

By now I only handled adding new friends that are registered in database.

This app is based on react-boilerplate

## License

This project is licensed under the MIT license, Copyright (c) 2017 Maximilian
Stoiber. For more information see `LICENSE.md`.
