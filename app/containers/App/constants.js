/*
 * AppConstants
 * Each action has a corresponding type, which the reducer knows and picks up on.
 * To avoid weird typos between the reducer and the actions, we save them as
 * constants here. We prefix them with 'yourproject/YourComponent' so we avoid
 * reducers accidentally picking up actions they shouldn't.
 *
 * Follow this format:
 * export const YOUR_ACTION_CONSTANT = 'yourproject/YourContainer/YOUR_ACTION_CONSTANT';
 */

export const DEFAULT_LOCALE = 'en';


export const FETCH_FRIENDS_PENDING_REQUESTS = 'app/main/fetch/friends/requests';
export const FETCH_FRIENDS = 'app/main/fetch/friends/list';

export const ADD_FRIEND_REQUEST = 'app/main/friends/add';
export const ADD_FRIEND_REQUEST_ERROR = 'app/main/friends/add/error';
export const ADD_FRIEND_REQUEST_SUCCESSFULL = 'app/main/friends/add/successfull';

export const POST_FRIENDS_REQUEST_STATUS = 'app/main/friends/status';


export const FETCH_FRIENDS_REQUEST_FINISHED = 'app/main/fetch/friends/list';
