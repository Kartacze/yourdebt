import {
  FETCH_FRIENDS_PENDING_REQUESTS,
  ADD_FRIEND_REQUEST,
  POST_FRIENDS_REQUEST_STATUS,
 } from './constants';


export function makeFriendsFetch() {
  return {
    type: ADD_FRIEND_REQUEST,
  };
}

export function addFriendAction(val) {
  return {
    type: ADD_FRIEND_REQUEST,
    data: val,
  };
}

export function fetchUserFriendsRequestsAction() {
  return {
    type: FETCH_FRIENDS_PENDING_REQUESTS,
  };
}

export function postFriendsRequestStatus(status, id) {
  return {
    type: POST_FRIENDS_REQUEST_STATUS,
    data: {status, id}
  };
}
