import { createSelector } from 'reselect';

const selectGlobal = (state) => state.get('global');

const makeSelectAuthorized = () => createSelector(
  selectGlobal,
  (globalState) => globalState.get('authorized')
);

const makeSelectSignInError = () => createSelector(
  selectGlobal,
  (globalState) => globalState.get('signin_error')
);

const makeSelectSignUpError = () => createSelector(
  selectGlobal,
  (globalState) => globalState.get('signup_error')
);

const selectPendingFriendsRequests = () => createSelector(
  selectGlobal,
  (globalState) => globalState.get('friends_requests')
);

const selectFriendsLoading = () => createSelector(
  selectGlobal,
  (globalState) => globalState.get('friends_loading')
);

const selectUsername = () => createSelector(
  selectGlobal,
  (globalState) => globalState.getIn(['user', 'email'])
);

const selectUserFriends = () => createSelector(
  selectGlobal,
  (globalState) => globalState.getIn(['user', 'friends'])
);

const makeSelectLocationState = () => {
  let prevRoutingState;
  let prevRoutingStateJS;

  return (state) => {
    const routingState = state.get('route'); // or state.route

    if (!routingState.equals(prevRoutingState)) {
      prevRoutingState = routingState;
      prevRoutingStateJS = routingState.toJS();
    }

    return prevRoutingStateJS;
  };
};

export {
  selectGlobal,
  makeSelectAuthorized,
  makeSelectSignInError,
  makeSelectLocationState,
  selectPendingFriendsRequests,
  selectFriendsLoading,
  selectUsername,
  selectUserFriends,
};
