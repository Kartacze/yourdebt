/**
 * Gets the repositories of the user from Github
 */

import { combineEpics } from 'redux-observable';
import { LOCATION_CHANGE } from 'react-router-redux';

import { Observable } from 'rxjs/Observable';
import { ajax } from 'rxjs/observable/dom/ajax';

import { FETCH_FRIENDS_PENDING_REQUESTS,
  FETCH_FRIENDS,
  ADD_FRIEND_REQUEST,
  ADD_FRIEND_REQUEST_ERROR,
  ADD_FRIEND_REQUEST_SUCCESSFULL,
  POST_FRIENDS_REQUEST_STATUS,
  FETCH_FRIENDS_REQUEST_FINISHED,
 } from './constants';

import {
  USER_DATA,
  USER_SIGNEDIN,
} from 'containers/Auth/Auth/constants';

const root_url = 'http://localhost:3090';


const addFriendEpic = (action$) =>
  action$.ofType(ADD_FRIEND_REQUEST)
    .mergeMap((action) => (
      ajax.post(`${root_url}/friends/add`, { buddy: action.data },
        { 'Content-Type': 'application/json', Authorization: localStorage.getItem('token') })
        .map((response) => {
          console.log(response.response.error);
          if (response.response.hasOwnProperty('error')) {
            return ({ type: ADD_FRIEND_REQUEST_ERROR, data: response.response.error });
          } else {
            return ({ type: ADD_FRIEND_REQUEST_SUCCESSFULL, data: response.response });
          }
        })
        .catch((e) => {
          return Observable.of({ type: ADD_FRIEND_REQUEST_ERROR, data: e.message });
        }))
      );

const getFriendRequests = (action$) =>
  action$.ofType(FETCH_FRIENDS_PENDING_REQUESTS)
    .mergeMap((action) => (
      ajax.get(`${root_url}/friends/requests`,
        { 'Content-Type': 'application/json', Authorization: localStorage.getItem('token') })
        .map((response) => {
          console.log('friends requests response', response.response.error);
          if (response.response.hasOwnProperty('error')) {
            return ({ type: 'ERROR_FRIENDS_REQUESTS', data: response.response.error });
          } else {
            return ({ type: FETCH_FRIENDS_REQUEST_FINISHED, data: response.response.resp });
          }
        })
        .catch((e) => {
          return Observable.of({ type: "ERROR_FRIENDS_REQUESTS", data: e.message });
        }))
      );


const postRequestStatusEpic = (action$) =>
  action$.ofType(POST_FRIENDS_REQUEST_STATUS)
    .mergeMap((action) => (
      ajax.post(`${root_url}/friends/requests/${action.data.id}`, { status: action.data.status },
        { 'Content-Type': 'application/json', Authorization: localStorage.getItem('token') })
        .map((response) => {
          console.log(response.response.error);
          if (response.response.hasOwnProperty('error')) {
            return ({ type: "POST_STATUS_REQUEST_ERROR", data: response.response.error });
          } else {
            return ({ type: USER_SIGNEDIN });
          }
        })
        .catch((e) => {
          return Observable.of({ type: "POST_STATUS_REQUEST_ERROR", data: e.message });
        }))
      );

const rootEpic = combineEpics(
  addFriendEpic,
  getFriendRequests,
  postRequestStatusEpic,
);

export default rootEpic;
