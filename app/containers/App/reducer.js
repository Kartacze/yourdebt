/*
 * AppReducer
 *
 * The reducer takes care of our data. Using actions, we can change our
 * application state.
 * To add a new action, add it to the switch statement in the reducer function
 *
 * Example:
 * case YOUR_ACTION_CONSTANT:
 *   return state.set('yourStateVariable', true);
 */

import { combineReducers } from 'redux'
import { fromJS } from 'immutable';

import {
  SIGNIN_USER,
  ERROR_SIGNIN_USER,
  ADD_FRIEND_REQUEST,
  ADD_FRIEND_REQUEST_ERROR,
  ADD_FRIEND_REQUEST_SUCCESSFULL,
  FETCH_FRIENDS_PENDING_REQUESTS,
  FETCH_FRIENDS_REQUEST_FINISHED
} from './constants';

import { USER_DATA } from 'containers/Auth/Auth/constants'

// The initial state of the App // every page should have its own reducer ...
const initialState = fromJS({
    authorized: false,
    error: '',
    friends_requests: [],
    friends_loading: false,
    user: {}
});

function rootReducer(state = initialState, action) {
  switch (action.type) {
    case USER_DATA:
      return state
        .set('user', fromJS(action.data))
    case 'SOME EXAMPLE':
      return state
        .set('signin_error', action.data)
        .set('authorized', false);
    case ADD_FRIEND_REQUEST_ERROR:
      return state
        .set('add_friend_error', action.data)
    case ADD_FRIEND_REQUEST_SUCCESSFULL:
      return state
        .set('add_friend_succ', true)
        .set('add_friend_error', '')
    case FETCH_FRIENDS_REQUEST_FINISHED:
      return state
        .set('friends_requests', action.data)
        .set('friends_loading', false);
    case FETCH_FRIENDS_PENDING_REQUESTS:
      return state
        .set('friends_loading', true);
    default:
      return state;
  }
}

export default rootReducer;
