import React, { Component } from 'react';
import { connect } from 'react-redux';

import { makeSelectAuthorized } from 'containers/Auth/Auth/selectors';
import { createStructuredSelector } from 'reselect';
import { browserHistory } from 'react-router';

export default function(ComposedComponent) {
  class Authentication extends Component {
    static contextTypes = {
      router: React.PropTypes.object
    }

    componentWillMount() {
      if (!this.props.authorized) {
        browserHistory.push('/');
      }
    }

    componentWillUpdate(nextProps) {
      if (!nextProps.authorized) {
        browserHistory.push('/');
      }
    }

    render() {
      return <ComposedComponent {...this.props}/>
    }
  }

  const mapStateToProps = createStructuredSelector({authorized: makeSelectAuthorized()});

  return connect(mapStateToProps)(Authentication);
}
