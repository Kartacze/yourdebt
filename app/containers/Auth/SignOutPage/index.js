import React from 'react';
import { Grid, Segment } from 'semantic-ui-react';

const SignOut = (props) => { // eslint-disable-line react/prefer-stateless-function
  return (
    <Grid centered columns={3} verticalAlign='middle' style={{ height: '80vh' }}>
      <Grid.Column>
        <Segment stacked>
          <p> We are extremly sad to see you go </p>
        </Segment>
      </Grid.Column>
    </Grid>
  );
};

export default SignOut;
