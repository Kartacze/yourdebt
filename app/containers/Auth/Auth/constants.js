

export const SIGNIN_USER = 'app/authentication/signInUser';
export const SIGNUP_USER = 'app/authentication/signUpUser';
export const SIGNOUT_USER = 'app/authentication/signOutUser';

export const USER_SIGNEDIN = 'app/authentication/SignedIn';
export const USER_SIGNEDOUT = 'app/authentication/userSignedOut';
export const AUTH_SIGNOUT_USER = 'app/authentication/signOutUser';

export const ERROR_SIGNIN_USER = 'app/authentication/SignIn/Error';
export const ERROR_SIGNUP_USER = 'app/authentication/SignUp/Error';

export const CHECK_USER_AUTH = 'app/authentication/CheckUserAuthorized';

export const USER_DATA = 'app/authentication/GetUserData'
