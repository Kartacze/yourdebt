import { createSelector } from 'reselect';

const selectAuth = (state) => state.get('auth');

const makeSelectAuthorized = () => createSelector(
  selectAuth,
  (globalState) => globalState.get('authorized')
);

const makeSelectSignInError = () => createSelector(
  selectAuth,
  (globalState) => globalState.get('signin_error')
);

const makeSelectSignUpError = () => createSelector(
  selectAuth,
  (globalState) => globalState.get('signup_error')
);

export {
  selectAuth,
  makeSelectAuthorized,
  makeSelectSignInError,
  makeSelectSignUpError,
};
