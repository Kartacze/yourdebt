import React, {Component} from 'react';
import {createStructuredSelector} from 'reselect';
import {connect} from 'react-redux';
import {Button, Menu} from 'semantic-ui-react';
import { Link, browserHistory } from 'react-router';

import { makeSelectAuthorized } from './selectors';

import { signInUser, signOutUser, signUpUser, checkUserAuth } from './actions';

class Auth extends Component { // eslint-disable-line react/prefer-stateless-function
  componentWillMount() {
    this.props.doCheckUserAuth();
  }

  static propTypes = {
    children: React.PropTypes.node
  };

  static contextTypes = {
    router: React.PropTypes.object
  };

  render() {
    const {authorized} = this.props;
    if (!authorized) {
      return (
        <Menu.Item>
          <Button primary onClick={(e) =>
              browserHistory.push('/signin')
            }>
            Sing In
          </Button>,
          <Button primary onClick={(e) =>
              browserHistory.push('signup')
            }>
            Sing Up
          </Button>
        </Menu.Item>
      );
    } else {
      return (
        <Menu.Item>
          <Button primary onClick={(e) =>{
              this.props.onSignOutUser();
              browserHistory.push('signout');
            }
            }>
            Sing Out
          </Button>
        </Menu.Item>
      )
    }
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    onSignOutUser: (e) => dispatch(signOutUser(e)),
    doCheckUserAuth: () => dispatch(checkUserAuth()),
  };
}

const mapStateToProps = createStructuredSelector({authorized: makeSelectAuthorized()});

export default connect(mapStateToProps, mapDispatchToProps)(Auth);
