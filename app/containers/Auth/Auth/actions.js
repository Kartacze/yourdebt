/*
 * Home Actions
 *
 * Actions change things in your application
 * Since this boilerplate uses a uni-directional data flow, specifically redux,
 * we have these actions which are the only way your application interacts with
 * your application state. This guarantees that your state is up to date and nobody
 * messes it up weirdly somewhere.
 *
 * To add a new Action:
 * 1) Import your constant
 * 2) Add a function like this:
 *    export function yourAction(var) {
 *        return { type: YOUR_ACTION_CONSTANT, var: var }
 *    }
 */

import {
  SIGNIN_USER,
  SIGNUP_USER,
  SIGNOUT_USER,
  CHECK_USER_AUTH,
} from 'containers/Auth/Auth/constants';

/**
 * Changes the input field of the form
 *
 * @param  {name} name The new text of the input field
 *
 * @return {object}    An action object with a type of CHANGE_USERNAME
 */

// need to change it to constants

export function signInUser(data) {
  return {
    type: SIGNIN_USER,
    data,
  };
}

export function signUpUser(data) {
  return {
    type: SIGNUP_USER,
    data,
  };
}

export function signOutUser(data) {
  return {
    type: SIGNOUT_USER,
    data,
  };
}

export function checkUserAuth() {
  return {
    type: CHECK_USER_AUTH,
  };
}
