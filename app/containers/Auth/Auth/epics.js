/**
 * Gets the repositories of the user from Github
 */

import { combineEpics } from 'redux-observable';
import {
  SIGNOUT_USER,
  SIGNIN_USER,
  SIGNUP_USER,
  USER_SIGNEDOUT,
  USER_SIGNEDIN,
  ERROR_SIGNIN_USER,
  ERROR_SIGNUP_USER,
  CHECK_USER_AUTH,
  USER_DATA,
} from './constants';
import { browserHistory } from 'react-router';

import { Observable } from 'rxjs/Observable';
import { ajax } from 'rxjs/observable/dom/ajax';

// need to be set manualy,
const root_url = 'http://localhost:3090';

const postSignInEpic = (action$) =>
  action$.ofType(SIGNIN_USER)
    .mergeMap((action) => (
      ajax.post(`${root_url}/signin`, { 'email': action.data.email,
        'password': action.data.password }, { 'Content-Type': 'application/json' })
      .map((response) => {
        console.log('response', response);
        localStorage.setItem('token', response.response.token);
        browserHistory.push('/');
        return ({ type: USER_SIGNEDIN, data: response });
      })
      .catch((e) => {
        // console.log('error', e);
        return Observable.of({ type: ERROR_SIGNIN_USER, data: e.message });
      }))
    );

const postSignUpEpic = (action$) =>
  action$.ofType(SIGNUP_USER)
    .mergeMap((action) => (
      ajax.post(`${root_url}/signup`, { 'email': action.data.email,
        'password': action.data.password }, { 'Content-Type': 'application/json' })
      .map((response) => {
        console.log('response', response);
        localStorage.setItem('token', response.response.token);
        browserHistory.push('/');
        return ({ type: USER_SIGNEDIN, data: response });
      })
      .catch((e) => {
        return Observable.of({ type: ERROR_SIGNUP_USER, data: e.message });
      }))
    );

const handleSingOutEpic = (action$) =>
  action$.ofType(SIGNOUT_USER)
    .delay(1000)
    .map((response) => {
      localStorage.removeItem('token');
      browserHistory.push('/');
      return ({ type: USER_SIGNEDOUT, data: response });
    });

const checkUserAuthEpic = (action$) =>
  action$.ofType(CHECK_USER_AUTH)
  .map((response) => {
    if (localStorage.getItem('token')) {
      return ({ type: USER_SIGNEDIN, data: response });
    } else return ({ type: 'EMPTY' }); // ???
  });

const getUserData = (action$) =>
  action$.ofType(USER_SIGNEDIN)
    .mergeMap((action) => (
      ajax.get(`${root_url}/user`, { Authorization: localStorage.getItem('token') })
      .map((response) => {
        return ({ type: USER_DATA, data: response.response });
      })
      .catch((e) => {
        return Observable.of({ type: ERROR_SIGNUP_USER, data: e.message }); // wrong error hangling
      }))
    );


const authEpic = combineEpics(
  postSignInEpic,
  postSignUpEpic,
  handleSingOutEpic,
  checkUserAuthEpic,
  getUserData,
);

export default authEpic;
