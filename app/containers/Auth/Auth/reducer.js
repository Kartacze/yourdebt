/*
 * AppReducer
 *
 * The reducer takes care of our data. Using actions, we can change our
 * application state.
 * To add a new action, add it to the switch statement in the reducer function
 *
 * Example:
 * case YOUR_ACTION_CONSTANT:
 *   return state.set('yourStateVariable', true);
 */

import { combineReducers } from 'redux'
import { fromJS } from 'immutable';

import {
  SIGNIN_USER,
  USER_SIGNEDIN,
  USER_SIGNEDOUT,
  ERROR_SIGNIN_USER,
  ERROR_SIGNUP_USER,
} from './constants';

// The initial state of the App
const initialState = fromJS({
    authorized: false,
    signin_error: '',
});

function authReducer(state = initialState, action) {
  switch (action.type) {
    case ERROR_SIGNIN_USER:
      return state
        .set('signin_error', action.data)
        .set('authorized', false);
    case ERROR_SIGNUP_USER:
      return state
        .set('signup_error', action.data)
        .set('authorized', false);
    case USER_SIGNEDIN:
      return state
        .set('authorized', true)
        .set('error', '');
    case USER_SIGNEDOUT:
      return state
        .set('authorized', false)
        .set('error', '');
    default:
      return state;
  }
}

export default authReducer;
