import React from 'react';
import { connect } from 'react-redux';
import { Grid, Form, Label, Button, Segment } from 'semantic-ui-react';
import { Field, reduxForm } from 'redux-form/immutable';
import { createStructuredSelector } from 'reselect';

import { makeSelectSignUpError } from 'containers/Auth/Auth/selectors';
import { signUpUser } from 'containers/Auth/Auth/actions';

const renderField = ({ input, label, type, meta: { touched, error } }) => (
  <div>
    <Form.Input {...input} type={type} placeholder={label} label={label} error={error && touched} />
    {touched && error && <Label style={{margin: '0 0 1em 1em'}}>{error}</Label>}
  </div>
);

const validate = (values) => {
  const errors = {}
  if (!values.get('password')) {
    errors.password = 'Required'
  } else if (values.get('password').length > 15) {
    errors.password = 'Must be 15 characters or less'
  }
  if (!values.get('email')) {
    errors.email = 'Required';
  } else if (
    !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.get('email'))
  ) {
    errors.email = 'Invalid email address';
  }
  if (values.get('password') != values.get('conf_password')) {
    errors.conf_password = 'Must have the same value';
  }
  return errors;
};

let SignUp = (props) => { // eslint-disable-line react/prefer-stateless-function

  const { handleSubmit, pristine, reset, submitting, onSignInUser, errorAuth } = props;

  const handleFormSubmit = (values) => {
    onSignInUser({ email: values.get('email'), password: values.get('password') });
  };

  return (
    <Grid centered columns={2} verticalAlign='middle' style={{ height: '80vh' }}>
      <Grid.Column>
        <Segment stacked >
          {errorAuth && <div>{errorAuth}</div>}
          <Form onSubmit={handleSubmit((values) => handleFormSubmit(values))}>
            <Field
              name="email"
              type="text"
              component={renderField}
              label="Email"
            />
            <Field
              name="password"
              type="password"
              component={renderField}
              label="Password"
            />
            <Field
              name="conf_password"
              type="password"
              component={renderField}
              label="Confirm Password"
            />
            <Button type="submit">Submit</Button>
          </Form>
        </Segment>
      </Grid.Column>
    </Grid>
  );
};

SignUp = reduxForm({
  form: 'signUpForm', // a unique identifier for this form
  validate,
})(SignUp);

export function mapDispatchToProps(dispatch) {
  return {
    onSignInUser: (evt) => dispatch(signUpUser(evt)),
  };
}

const mapStateToProps = createStructuredSelector({
  errorAuth: makeSelectSignUpError(),
});

export default connect(mapStateToProps, mapDispatchToProps)(SignUp);
