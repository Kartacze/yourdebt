import React from 'react';
import { connect } from 'react-redux';
import { Grid, Form, Label, Button, Segment, Input, Message } from 'semantic-ui-react';
import { Field, reduxForm } from 'redux-form/immutable';
import { createStructuredSelector } from 'reselect';

import { makeSelectSignInError } from 'containers/Auth/Auth/selectors';
import { signInUser } from 'containers/Auth/Auth/actions';

const renderField = ({ input, label, type, meta: { touched, error } }) => (
    <Form.Field inline error={error && touched} >
      <label style={{width: '90px'}} >{label}</label>
      <input {...input} type={type} placeholder={label} error />
      {touched && error && <Label pointing="left">{error}</Label>}
    </Form.Field>
);


const validate = (values) => {
  const errors = {}
  if (!values.get('password')) {
    errors.password = 'Required'
  } else if (values.get('password').length > 15) {
    errors.password = 'Must be 15 characters or less'
  } else if (values.get('password').length < 3) {
    errors.password = 'Must be 4 or more characters'
  }
  if (!values.get('email')) {
    errors.email = 'Required'
  } else if (
    !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.get('email'))
  ) {
    errors.email = 'Invalid email address'
  }
  return errors;
};

let SignIn = ({ handleSubmit, pristine, reset, submitting, onSignInUser, errorAuth }) => { // eslint-disable-line react/prefer-stateless-function

  const handleFormSubmit = (values) => {
    onSignInUser({ email: values.get('email'), password: values.get('password') });
  };

  return (
    <Grid centered columns={2} verticalAlign='middle' style={{ height: '80vh' }}>
      <Grid.Column>
        <Segment stacked>
          {errorAuth && <Message
            error
            header='Error'
            content={errorAuth}
          />}
          <Form onSubmit={handleSubmit((values) => handleFormSubmit(values))}>
            <Field
              name="email"
              type="text"
              component={renderField}
              label="Email"
            />
            <Field
              name="password"
              type="password"
              component={renderField}
              label="Password"
            />

            <Button type="submit">Submit</Button>
          </Form>
        </Segment>
      </Grid.Column>
    </Grid>
  );
};

SignIn = reduxForm({
  form: 'signInForm', // a unique identifier for this form
  validate,
})(SignIn);

export function mapDispatchToProps(dispatch) {
  return {
    onSignInUser: (evt) => dispatch(signInUser(evt)),
  };
}

const mapStateToProps = createStructuredSelector({
  errorAuth: makeSelectSignInError(),
});

SignIn = connect(mapStateToProps, mapDispatchToProps)(SignIn);

export default SignIn;
