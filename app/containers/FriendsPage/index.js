import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import Authentication from 'containers/Auth/ReqAuth';
import AddFriend from 'components/AddFriend'
import FriendsList from 'components/FriendsList'

import {
  makeFriendsFetch,
  addFriendAction,
  fetchUserFriendsRequestsAction,
  postFriendsRequestStatus,
} from 'containers/App/actions';

import {
  selectPendingFriendsRequests,
  selectFriendsLoading,
  selectUsername,
  selectUserFriends,
} from 'containers/App/selectors';

import { Segment, Grid, Form, Button } from 'semantic-ui-react';

import PendingFriends from 'components/PendingFriends';

class FriendsPage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state={
      addfriend: false,
      pending_req: false,
    }

    this.handleAccept = this.handleAccept.bind(this);
    this.handleDecline = this.handleDecline.bind(this);
  }

  handlePendingRequest() {
    const { pending_req } = this.state;
    this.setState({pending_req: !pending_req, addfriend: false});
    if(!pending_req) {
      this.props.fetchFriendsRequests();
    }
  }

  handleAccept(id) {
    this.props.friendsStatus('accept', id);
    this.props.fetchFriendsRequests();
  }

  handleDecline(id) {
    this.props.friendsStatus('decline', id);
    this.props.fetchFriendsRequests();
  }

  handleAddFriend() {
    const { addfriend } = this.state;
    this.setState({addfriend: !addfriend, pending_req: false});
  }

  render() {
    const { username } = this.props;

    const reqPart = this.state.pending_req && <PendingFriends
      list={this.props.pending_requests}
      load={this.props.friends_loading}
      accept={this.handleAccept}
      decline={this.handleDecline}
      username={username}
      />;
    console.log('pending list', this.props.pending_requests);
    return (
      <Grid>
      <Grid.Row columns={1}>
        <Grid.Column>
          <h2 style={{display: 'inline'}}> Friends list </h2>
          <FriendsList list={ this.props.friends || [] } />
        </Grid.Column>
      </Grid.Row>

        <Grid.Row columns={1}>
          <Grid.Column>
          <h2 style={{display: 'inline'}}> Add Friend </h2>
          <Button style={{display: 'inline'}} onClick={ () => this.handleAddFriend() }>
            Click
          </Button>
          { this.state.addfriend && <AddFriend action={ this.props.addfriend }/> }
          </Grid.Column>
        </Grid.Row>

        <Grid.Row columns={1}>
          <Grid.Column>
          <h2 style={{display: 'inline'}}> Pending requests </h2>
          <Button style={{display: 'inline'}} onClick={() => this.handlePendingRequest()}>
            Click
          </Button>
          { reqPart }
          </Grid.Column>
        </Grid.Row>
      </Grid>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    onClickedSomething: () => dispatch(makeFriendsFetch()),
    addfriend: (e) => dispatch(addFriendAction(e)),
    fetchFriendsRequests: () => dispatch(fetchUserFriendsRequestsAction()),
    friendsStatus: (status, id) => dispatch(postFriendsRequestStatus(status, id)),
  };
}

const mapStateToProps = createStructuredSelector({
  pending_requests: selectPendingFriendsRequests(),
  friends_loading: selectFriendsLoading(),
  username: selectUsername(),
  friends: selectUserFriends(),
});

export default Authentication(connect(mapStateToProps, mapDispatchToProps)(FriendsPage))
