import React from 'react';
import { Item, Segment } from 'semantic-ui-react';

const FriendsList = ({ list, load }) => { // eslint-disable-line react/prefer-stateless-function
  return (
    <Segment >
      <Item.Group>
        { list.map( e =>
          <Item>
            <Item.Content>
              <Item.Header as='a'>{e}</Item.Header>
              <Item.Meta>Description</Item.Meta>
              <Item.Description>
                <p> your friend </p>
              </Item.Description>
            </Item.Content>
          </Item>
        )}
      </Item.Group>
    </Segment>
  );
};

export default FriendsList;
