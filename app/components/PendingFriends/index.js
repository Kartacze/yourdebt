import React from 'react';
import { Card, Form, Label, Button, Segment, Input, Message, Item, Icon, Divider } from 'semantic-ui-react';


const PendingFriends = ({ list, load, username, accept, decline }) => { // eslint-disable-line react/prefer-stateless-function
  const makeCards = type => e =>
    <Card>
      <Card.Content>
        <Card.Header>
          {e[type]}
        </Card.Header>
        <Card.Description>
          Steve wants to add you to the group <strong>best friends</strong>
        </Card.Description>
      </Card.Content>
      <Card.Content extra>
        <div className='ui two buttons'>
          <Button basic color='green' onClick={() => accept(e._id)}>Approve</Button>
          <Button basic color='red' onClick={() => decline(e._id)}>Decline</Button>
        </div>
      </Card.Content>
    </Card>;

  const makeItems = e =>
    <Item>
      <Item.Content>
        <Item.Header as='a'>{e.new}</Item.Header>
        <Item.Meta>
          <span className='cinema'>Some description</span>
          <Button primary floated='right'>
            Cancel
            <Icon name='right chevron' />
          </Button>
        </Item.Meta>
        <Item.Description></Item.Description>
      </Item.Content>
    </Item>;

  return (
    <Segment loading={load} >
      <h3> Waiting for your approve </h3>
      <Card.Group>
        {list.filter(e => (e.new === username)).map(makeCards('email'))}
      </Card.Group>
      <h3> Your requests </h3>
      <Divider />
      <Item.Group divided>
        {list.filter(e => (e.new !== username)).map(makeItems)}
      </Item.Group>
    </Segment>
  );
};

export default PendingFriends;
