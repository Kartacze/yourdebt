import React from 'react';
import { Grid, Form, Label, Button, Segment, Input, Message } from 'semantic-ui-react';

import { Field, reduxForm } from 'redux-form/immutable';
import { createStructuredSelector } from 'reselect';

const renderField = ({ input, label, type, meta: { touched, error } }) => (
    <Form.Field error={error && touched} >
      <label>{label}</label>
      <input {...input} type={type} placeholder={label} error />
      {touched && error && <Label pointing="left">{error}</Label>}
    </Form.Field>
);

const validate = (values) => {
  const errors = {};
  if (!values.get('email')) {
    errors.email = 'Required';
  } else if (
    !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.get('email'))
  ) {
    errors.email = 'Invalid email address';
  }
  return errors;
};


const addFriend = ({ handleSubmit, action, error }) => {
  return (
    <Segment raised>
      <Form onSubmit={ handleSubmit((val) => action(val.get('email'))) }>
        <Field
          name="email"
          type="text"
          component={renderField}
          label="Add friend"
          placeholder='example@simple.pl'
        />
        { error && <Message negative>
            <Message.Header>Error</Message.Header>
            <p>{error}</p>
          </Message> }
        <Button type="submit">Submit</Button>
      </Form>
    </Segment>
  );
}

const AddFriend = reduxForm({
  form: 'addFriend', // a unique identifier for this form
  validate,
})(addFriend);

export default AddFriend;
