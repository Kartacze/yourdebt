import React, { Component } from 'react';
import { Button, Dropdown, Menu, Icon } from 'semantic-ui-react';

import { browserHistory } from 'react-router';

import Auth from 'containers/Auth/Auth';

export default class MyMenu extends Component { // eslint-disable-line react/prefer-stateless-function
  state = { activeItem: 'home' }

  handleItemClick = (e, { name, label }) => {
    this.setState({ activeItem: name });
    browserHistory.push(label);
  }

  render() {
    const { activeItem } = this.state;

    return (
      <Menu size="tiny" stackable>
        <Menu.Item
          name="home" label="/"
          active={activeItem === 'home'} onClick={this.handleItemClick}
        />
        <Menu.Item
          name="messages" label="/messages"
          active={activeItem === 'messages'} onClick={this.handleItemClick}
        />
        <Menu.Item
          name="friends" label="/friends"
          active={activeItem === 'friends'} onClick={this.handleItemClick}
        />

        <Menu.Menu position="right">
          <Menu.Item name='settings' label="/settings"
            active={activeItem === 'settings'} onClick={this.handleItemClick}>
            <Icon name='cubes' />
            Settings
          </Menu.Item>
          <Auth />
        </Menu.Menu>
      </Menu>
    );
  }
}
